<?php

use Illuminate\Database\Seeder;

class BackpackSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'ikhsanblog@yahoo.com',
            'password' => '$2y$10$oeAiDjx/J.IQO3VanUzVJ..3PO6nmrCOWa4PhkpWNfM/nWYm7BSJ2',
            ]);
    }
}
