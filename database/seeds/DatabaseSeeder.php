<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(bukuSeeder::class); 
        $this->call(bukugenreseeder::class); 
        $this->call(DeviceElektronikSeeder::class); 
        $this->call(DeviceElektronikTypeSeeder::class); 
        $this->call(PeminjamanSeeder::class); 
        $this->call(BackpackSeeder::class); 
    }
}
