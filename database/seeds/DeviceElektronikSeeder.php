<?php

use Illuminate\Database\Seeder;

class DeviceElektronikSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('deviceelektronik')->insert([
            'id' => '2',
            'type' => 'test2',
            'brand' => 'test2',
            'series' => 'test2',
            'year' => 'test2',
             ]);
    }
}
