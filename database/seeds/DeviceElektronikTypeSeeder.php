<?php

use Illuminate\Database\Seeder;

class DeviceElektronikTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('deviceelektroniktype')->insert(array(
            array(
              'id' => '1',
              'type' => 'Notebook',
            ),
            array(
              'id' => '2',
              'type' => 'Tablet',
            ),
            array(
                'id' => '3',
                'type' => 'Router',
              ),
         
              array(
                'id' => '4',
                'type' => 'Switch',
              ),
         
              array(
                'id' => '5',
                'type' => 'RTU',
              ),
          ));
    }
}
