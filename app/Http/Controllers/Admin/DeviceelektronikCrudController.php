<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\DeviceelektronikRequest as StoreRequest;
use App\Http\Requests\DeviceelektronikRequest as UpdateRequest;

/**
 * Class DeviceelektronikCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class DeviceelektronikCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Deviceelektronik');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/deviceelektronik');
        $this->crud->setEntityNameStrings('deviceelektronik', 'deviceelektroniks');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        $this->crud->setFromDb();

        $this->crud->addField([
            // 1-n relationship
            'label' => "Pilih Device", 
            'type' => "select",
            'name' => 'type', 
            'entity' => 'Deviceelektroniktype', 
            'attribute' => "type", 
            'model' => "App\Models\Deviceelektroniktype", 
        ]);


       // $this->crud->enableExportButton();

     
        // add asterisk for fields that are required in DeviceelektronikRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }
  

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
