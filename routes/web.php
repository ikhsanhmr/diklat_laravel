<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/halo', function () {
    return 'Halo Gaess';
});

Route::get('/contoh2/{nama?}', function ($nama='Ikhsan') {
    return 'Selamat Datang '.$nama;
});

Route::get('/halaman', function () {
    return view('halaman');
});